from math import sqrt, ceil, floor


with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()

map = {}

for line in lines:
    split_line = line.split(":")
    variable = split_line[0]
    values = "".join(split_line[1].split()).split()
    map[variable] = {"name":variable, "values":[int(v) for v in values]}

times = map["Time"]["values"]
record_distances = map["Distance"]["values"]
print (times)
print(record_distances)
sum =1
for i,time in enumerate(times):
    min_pressing =0
    max_pressing = time
    record_distance = record_distances[i]
    
    # 1. 
    # speed = pressing
    # 2.
    # distance = speed*(time-pressing)
    # 3.
    # record_distance = distance
    # 
    # x = (-b+-sqrt(b*b-4*a*c))/(2*a)
    # =>
    # record_distance = pressing*(time-pressing)
    # record_distance = -pressing²+time*pressing
    # => 
    # -pressing²+time*pressing-record_distance
    # =>
    # a = -1
    # b = time
    # c =-record_distance

    pressing0 = floor((-time +sqrt(time*time-4*(-1)*(-record_distance)))/(2*(-1))+1)
    pressing1 = ceil((-time -sqrt(time*time-4*(-1)*(-record_distance)))/(2*(-1))-1)
    print("solutions "+ str(pressing0)+ " and "+str(pressing1))
    sum *= (pressing1-pressing0+1)

print(sum)