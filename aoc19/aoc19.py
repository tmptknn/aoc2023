with open("input.txt") as f:
    file_content = f.read().split("\n\n")

workflowsdata = file_content[0]
data = file_content[1]

parts = []
for row in data.split("\n"):
    part = {}
    for key_and_value in row[1:-1].split(","):
        key_and_value = key_and_value.split("=")
        key = key_and_value[0]
        value = key_and_value[1]
        part[key] = int(value)
    parts.append(part)

print(parts)

workflows = {}

for workflow in workflowsdata.split("\n"):
    name_index = workflow.find("{")
    name = workflow[:name_index]
    rules = workflow[name_index + 1 : -1]
    wfr = []
    for rule in rules.split(","):
        if rule.find("<") != -1:
            wfr.append(
                {
                    "state": "less",
                    "next": rule.split(":")[-1],
                    "key": rule.split("<")[0],
                    "value": int(rule.split("<")[1].split(":")[0]),
                }
            )
        elif rule.find(">") != -1:
            wfr.append(
                {
                    "state": "more",
                    "next": rule.split(":")[-1],
                    "key": rule.split(">")[0],
                    "value": int(rule.split(">")[1].split(":")[0]),
                }
            )
        else:
            wfr.append({"next": rule})

        print(wfr)
    workflows[name] = wfr

print(workflows)


def process_parts(workflows, parts):
    sum = 0
    for part in parts:
        next = "in"
        used = []
        print(part)
        while True:
            print(next)
            if next == "A":
                sum += part["x"] + part["m"] + part["a"] + part["s"]
                break
            if next == "R":
                break
            if next in used:
                print("lost in process")
                break
            else:
                used.append(key)

            wf = workflows[next]
            print(wf)

            for instruction in wf:
                print(instruction)
                if "state" not in instruction.keys():
                    print("hello")
                    next = instruction["next"]
                    break
                elif instruction["state"] == "less":
                    print(instruction["key"])
                    if part[instruction["key"]] < instruction["value"]:
                        next = instruction["next"]
                        break
                elif instruction["state"] == "more":
                    if part[instruction["key"]] > instruction["value"]:
                        next = instruction["next"]
                        break
                else:
                    print(instruction)
    return sum


print(process_parts(workflows, parts))
