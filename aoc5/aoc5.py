with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()
map_list = []
map_map = {}

for line in lines:
    if line.startswith("seeds"):
        seeds = [int(seed) for seed in line.split(": ")[1].split()]
        print(seeds)

for i, line in enumerate(lines):
    if line.endswith(" map:"):
        map = {"name": line.split(" map:")[0], "starts": i + 1}
        map_list.append(map)
        from_to = map["name"].split("-to-")
        from_type = from_to[0]
        to_type = from_to[1]
        map["from"] = from_type
        map["to"] = to_type
        map_map[from_type] = map

for map in map_list:
    current = map["starts"]
    mappings = []

    while True:
        line = lines[current]
        temp = [int(n) for n in line.split()]
        current += 1
        if line == "" or current >= len(lines):
            break
        mappings.append(
            {
                "destination range start": temp[0],
                "source range start": temp[1],
                "length": temp[2],
            }
        )
    map["mappings"] = mappings

lowest = None

for seed in seeds:
    current_map = map_map["seed"]
    value = seed
    while True:
        print(current_map["to"])
        print(value)
        mappings = current_map["mappings"]
        for mapping in mappings:
            start = mapping["source range start"]
            destination = mapping["destination range start"]
            l = mapping["length"]
            if value >= start and value <= start + l:
                value = value + destination - start
                break
        if current_map["to"] == "location":
            break
        current_map = map_map[current_map["to"]]

    print(value)
    if lowest is None or lowest > value:
        lowest = value
print(lowest)
