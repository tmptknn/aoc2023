with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()
map_list = []
map_map = {}

for line in lines:
    if line.startswith("seeds"):
        seeds = []
        seedsline = [int(seed) for seed in line.split(": ")[1].split()]
        for i in range(0, int(len(seedsline) / 2)):
            seeds.append({"start": seedsline[i * 2], "length": seedsline[i * 2 + 1]})
        # print(seeds)

for i, line in enumerate(lines):
    if line.endswith(" map:"):
        map = {"name": line.split(" map:")[0], "starts": i + 1}
        map_list.append(map)
        from_to = map["name"].split("-to-")
        from_type = from_to[0]
        to_type = from_to[1]
        map["from"] = from_type
        map["to"] = to_type
        map_map[from_type] = map

for map in map_list:
    current = map["starts"]
    mappings = []

    while True:
        line = lines[current]
        temp = [int(n) for n in line.split()]
        current += 1
        if line == "" or current >= len(lines):
            break
        mappings.append(
            {
                "destination range start": temp[0],
                "source range start": temp[1],
                "length": temp[2],
            }
        )
    map["mappings"] = mappings

lowest = None

for seed in seeds:
    sstart = seed["start"]
    send = sstart + seed["length"]

    current_map = map_map["seed"]
    values = [[sstart, seed["length"]]]
    while True:
        mappings = current_map["mappings"]
        new_values = []
        for vvalue in values:
            stack = [vvalue]
            while True:
                value = stack.pop()
                hits_mapping = False
                for mapping in mappings:
                    start = mapping["source range start"]
                    destination = mapping["destination range start"]
                    l = mapping["length"]
                    if (
                        value[0] < start + l and value[0] + value[1] - 1 >= start
                    ):  # overlapping ranges
                        hits_mapping = True
                        if value[0] >= start and value[0] + value[1] <= start + l:
                            new_values.append(
                                [value[0] + destination - start, value[1]]
                            )
                        elif value[0] < start and value[0] + value[1] <= start + l:
                            new_values.append(
                                [destination, value[0] + value[1] - start]
                            )
                            stack.append([value[0], start - value[0]])
                        elif value[0] >= start and value[0] + value[1] > start + l:
                            new_values.append(
                                [value[0] + destination - start, start + l - value[0]]
                            )
                            stack.append([start + l, value[0] + value[1] - (start + l)])
                        elif value[0] < start and value[0] + value[1] > start + l:
                            new_values.append([destination, l])
                            stack.append([value[0], start - value[0]])
                            stack.append([start + l, value[0] + value[1] - (start + l)])

                if not hits_mapping:
                    new_values.append(value)
                if len(stack) == 0:
                    break

        values = new_values
        if current_map["to"] == "location":
            break
        current_map = map_map[current_map["to"]]
    for value in values:
        if lowest is None or lowest > value[0]:
            lowest = value[0]


print(lowest)
