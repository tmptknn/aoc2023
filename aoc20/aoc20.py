with open("input.txt") as f:
    file_content = f.read().split("\n")

modules = {}

for line in file_content:
    split_line = line.split(" -> ")
    targets_string = split_line[1]
    module_string = split_line[0]
    targets = targets_string.split(", ")
    moduletype = "broadcast"
    modulename = module_string
    if module_string[0] == "%":
        moduletype = "flipflop"
        modulename = module_string[1:]
    elif module_string[0] == "&":
        moduletype = "conjunction"
        modulename = module_string[1:]
    modules[modulename] = {"name": modulename, "type": moduletype, "targets": targets}

print(modules)


def init(modules):
    for key in modules.keys():
        module = modules[key]
        if module["type"] == "flipflop":
            module["state"] = 0
        for target in module["targets"]:
            if target not in modules.keys():
                continue
            target_module = modules[target]
            if target_module["type"] == "conjunction":
                if "state" not in target_module.keys():
                    target_module["state"] = {}
                target_module["state"][module["name"]] = 0


init(modules)

print(modules)

# signals = [{"target": "broadcaster", "signal": 0, "from": "button"}]


def process_signals(modules, signals):
    high_signals = 0
    low_signals = 0
    while len(signals) != 0:
        signal = signals.pop(0)
        if signal["signal"] == 0:
            low_signals += 1
        else:
            high_signals += 1
        print(signal)
        if signal["target"] not in modules.keys():
            continue
        module = modules[signal["target"]]
        if module["type"] == "broadcast":
            for target in module["targets"]:
                signals.append(
                    {
                        "target": target,
                        "signal": signal["signal"],
                        "from": module["name"],
                    }
                )
        elif module["type"] == "flipflop":
            if signal["signal"] == 0:
                if module["state"] == 0:
                    for target in module["targets"]:
                        signals.append(
                            {"target": target, "signal": 1, "from": module["name"]}
                        )
                    module["state"] = 1
                else:
                    for target in module["targets"]:
                        signals.append(
                            {"target": target, "signal": 0, "from": module["name"]}
                        )
                    module["state"] = 0

        elif module["type"] == "conjunction":
            module["state"][signal["from"]] = signal["signal"]
            pulse = 0
            for key in module["state"].keys():
                if module["state"][key] == 0:
                    pulse = 1
            for target in module["targets"]:
                signals.append(
                    {"target": target, "signal": pulse, "from": module["name"]}
                )
        print(signals)

    return low_signals, high_signals


lows = 0
highs = 0
for i in range(1000):
    low_signals, high_signals = process_signals(
        modules, [{"target": "broadcaster", "signal": 0, "from": "button"}]
    )
    lows += low_signals
    highs += high_signals

print(lows)
print(highs)
print(lows * highs)
