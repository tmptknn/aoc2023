import math

with open("input.txt") as f:
    file_content = f.read().split("\n")

lines = []
for row in file_content:
    springs_and_count = row.split(" ")
    springs = springs_and_count[0]
    counts = springs_and_count[1]
    numbers = [int(count) for count in counts.split(",")]
    symbols = [symbol for symbol in springs]
    lines.append({"symbols": symbols, "n": numbers})

print(lines)


def check_row(symbols, stack, numbers):
    pline = []
    for i, symbol in enumerate(symbols):
        hit = False
        for j, n in enumerate(stack):
            if i >= n and i < n + numbers[j]:
                hit = True

        if symbol == "#" and not hit:
            return False
        if hit:
            pline += "I"
        else:
            pline += " "
    print("".join(symbols))
    print("".join(pline))
    return True


def find_solutions(symbols, numbers):
    current_position = 0
    current_number = 0
    stack = []
    count = 0
    while True:
        fit = True
        for i in range(numbers[current_number]):
            symbol = symbols[current_position + i]
            if symbol == ".":
                fit = False
                break

        if fit:
            stack.append(current_position)
            current_position += numbers[current_number] + 1
            current_number += 1

            if len(stack) == len(numbers):
                # print(stack)

                if check_row(symbols, stack, numbers):
                    print("")
                    count += 1

        else:
            current_position += 1

        while current_number >= len(numbers) or current_position + numbers[
            current_number
        ] > len(symbols):
            if len(stack) > 0:
                current_position = stack.pop() + 1
                current_number -= 1
            else:
                return count


sum = 0
for line in lines:
    symbols = line["symbols"]
    numbers = line["n"]
    print("".join(symbols))
    print(",".join([str(n) for n in numbers]))
    sum += find_solutions(symbols, numbers)

print(sum)
