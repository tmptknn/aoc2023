from functools import cache
import math

with open("input.txt") as f:
    file_content = f.read().split("\n")

lines = []
for row in file_content:
    springs_and_count = row.split(" ")
    springs = springs_and_count[0]
    counts = springs_and_count[1]
    numbers = [int(count) for count in counts.split(",")]
    symbols = [symbol for symbol in springs]
    sum = 0
    cumulative_sum = []
    for i in range(len(numbers) - 1, -1, -1):
        cumulative_sum.insert(0, sum)
        sum += numbers[i] + 1
    lines.append({"symbols": symbols, "n": numbers, "sum": cumulative_sum})

print(lines)
unfolded_lines = []
for line in lines:
    symbols2 = []
    numbers2 = []
    for i in range(5):
        symbols2.append("".join(line["symbols"]))
        numbers2.extend(line["n"])

    sum = 0
    cumulative_sum = []
    for i in range(len(numbers2) - 1, -1, -1):
        cumulative_sum.insert(0, sum)
        sum += numbers2[i] + 1

    symbols2 = [symbol for symbol in "?".join(symbols2)]
    unfolded_lines.append({"symbols": symbols2, "n": numbers2, "sum": cumulative_sum})

# print(unfolded_lines)

lines = unfolded_lines


def check_row(symbols, stack, numbers):
    pline = []
    for i, symbol in enumerate(symbols):
        hit = False
        for j, n in enumerate(stack):
            if i >= n and i < n + numbers[j]:
                hit = True

        if symbol == "#" and not hit:
            return False
        if hit:
            pline += "I"
        else:
            pline += " "
    # print("".join(symbols))
    # print("".join(pline))
    return True


def find_solutions_broken(symbol, symbols, numbers, count):
    print("")
    print("symbol " + symbol)
    print("count " + str(count))
    print("".join(symbols) + " " + "".join([str(numb) for numb in numbers]))

    if len(symbols) == 0:
        print("solution")
        return 1

    if count > 0:
        if symbol == "#":
            return find_solutions(symbols[0], symbols[1:], numbers, count - 1)
        if symbol == ".":
            print("töks")
            return 0
        if symbol == "?":
            return find_solutions(".", symbols, numbers, count) + find_solutions(
                "#", symbols, numbers, count - 1
            )

    if len(numbers) == 0:
        print("solution")
        return 1

    if symbol == ".":
        return find_solutions(symbols[0], symbols[1:], numbers, count)
    else:
        return find_solutions(symbol, symbols, numbers[1:], numbers[0])


def find_solutions_unoptimized(symbols, numbers, sums, count, symbol):
    #  print(solution + "X")

    if len(symbols) == 0:
        if len(numbers) > 0:
            return 0
        if count > 0:
            return 0
        # print("")
        # print(solution)
        # print("")
        return 1
    #     print("solution")
    #       print(solution)
    #     return 1

    if len(sums) > 0 and len(symbols) < sums[0]:
        return 0

    if symbol == "?":  ## and (len(numbers) > 0 or count > 0):
        return find_solutions(symbols, numbers, sums, count, "#") + find_solutions(
            symbols, numbers, sums, count, "."
        )
    # print("symbol " + symbol)

    if count > 0:
        if symbol == ".":
            return 0
        if symbol == "#":
            add = 0
            if count == 1:
                if symbols[0] == "#":
                    return 0
                if len(symbols) >= 2:
                    add = 1

            return find_solutions(
                symbols[add + 1 :],
                numbers,
                sums,
                count - 1,
                symbols[add],
            )

    if symbol == ".":
        return find_solutions(symbols[1:], numbers, sums, count, symbols[0])
    if symbol == "#":
        if len(numbers) == 0 and count == 0:
            return 0
        return find_solutions(symbols, numbers[1:], sums[1:], numbers[0], symbol)


def find_solutions(symbols, numbers, sums):
    #  print(solution + "X")
    len_symbols = len(symbols)
    len_numbers = len(numbers)

    @cache
    def recursion(si, ni, count, symbol):
        if si == len_symbols:
            if ni < len_numbers:
                return 0

            if count > 0:
                return 0

            return 1

        if ni < len_numbers and len_symbols - ni < sums[ni]:
            return 0

        if symbol == "?":
            return recursion(si, ni, count, "#") + recursion(si, ni, count, ".")

        if count > 0:
            if symbol == ".":
                return 0
            if symbol == "#":
                add = 0
                if count == 1:
                    if symbols[si] == "#":
                        return 0
                    if len_symbols - si >= 2:
                        add = 1

                return recursion(
                    si + add + 1,
                    ni,
                    count - 1,
                    symbols[si + add],
                )

        if symbol == ".":
            return recursion(si + 1, ni, count, symbols[si])
        if symbol == "#":
            if ni == len_numbers and count == 0:
                return 0
            return recursion(si, ni + 1, numbers[ni], symbol)

    return recursion(0, 0, 0, ".")


def find_solutions_old(symbols, numbers, sums):
    current_position = 0
    current_number = 0
    stack = []
    count = 0
    while True:
        fit = True
        for i in range(numbers[current_number]):
            symbol = symbols[current_position + i]
            if symbol == ".":
                fit = False
                break

        if fit:
            stack.append(current_position)
            current_position += numbers[current_number] + 1
            current_number += 1

            if len(stack) == len(numbers):
                # print(stack)

                if check_row(symbols, stack, numbers):
                    # print("")
                    count += 1

        else:
            current_position += 1

        while current_number >= len(numbers) or current_position + numbers[
            current_number
        ] + sums[current_number] > len(symbols):
            if len(stack) > 0:
                current_position = stack.pop() + 1
                current_number -= 1
            else:
                return count


sum = 0
for i, line in enumerate(lines):
    symbols = line["symbols"]
    symbols.append(".")
    numbers = line["n"]
    sums = line["sum"]
    print(" " + "".join(symbols))
    print(",".join([str(n) for n in numbers]))
    count = find_solutions(symbols, numbers, sums)
    print("line " + str(i + 1) + "/" + str(len(lines)))
    print("solutions count " + str(count))
    print("")
    sum += count

print(sum)
