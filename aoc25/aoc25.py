import copy
import json
import math
import random
from itertools import combinations

with open("input.txt") as f:
    lines = f.read().split("\n")

wire_graph = []
wire_map = {}
wires = set()
wireso = []
wires_map = {}
dual_graph = {}
dual_graph_2 = {}
dual_list = []
for line in lines:
    name = line.split(": ")[0]
    connections = line.split(": ")[1].split()
    node = {"name": name, "connections": connections, "wires": [], "old_nodes": [name]}

    for connection in connections:
        nodes = [connection, name]
        nodes.sort()
        nodes = (nodes[0], nodes[1])
        wires.add(nodes)
        node["wires"].append(nodes)
        if connection in wire_map.keys():
            nodeb = wire_map[connection]

        else:
            nodeb = {
                "name": connection,
                "connections": [],
                "wires": [],
                "old_nodes": [connection],
            }
            wire_graph.append(nodeb)
            wire_map[connection] = nodeb
        nodeb["connections"].append(name)
        nodeb["wires"].append(nodes)

    wire_graph.append(node)
    wire_map[name] = node
# print(wire_graph)
for index, wire in enumerate(list(wires)):
    a = wire[0]
    b = wire[1]
    w = {"name": index, "connections": [], "wire": wire}
    dual_list.append(w)
    dual_graph[index] = w
    dual_graph_2[wire] = w

for n in wire_graph:
    n["wire_ids"] = []
    for w in n["wires"]:
        n["wire_ids"].append(dual_graph_2[w])
print(wire_graph)
# for w in dual_list:
#     wire = w["wire"]
#     a = wire_map[wire[0]]
#     b = wire_map[wire[1]]
#     for wireb in a["wires"]:
#         if wireb != wire:
#             w2 = dual_graph_2[wireb]
#             w["connections"].append(w2)
#     for wirec in b["wires"]:
#         if wirec != wire:
#             w3 = dual_graph_2[wirec]
#             w["connections"].append(w3)

wire_graph.sort(key=lambda n: len(n["connections"]))
for n in wire_graph:
    print(str(n["name"]) + " " + str(len(n["connections"])))

dual_list.sort(key=lambda n: len(n["connections"]), reverse=False)
for w in dual_list:
    print(str(w["wire"]) + " " + str(len(w["connections"])))

minc = len(dual_list[0]["connections"])
wires_cut = [n for n in dual_list if minc == len(n["connections"])]
print("Wires")
for w in wires_cut:
    print(w["wire"])


def calculate_nodes(wire_map, start, wires_cut):
    suma = 0
    current = wire_map[start]
    # print(start)
    if "visited" not in current.keys():
        current["visited"] = True
        for connection in current["connections"]:
            nodes = [connection, start]
            # print(str(nodes))
            nodes.sort()
            nodes = (nodes[0], nodes[1])
            if nodes not in wires_cut:
                suma += calculate_nodes(wire_map, connection, wires_cut)

        return suma + 1
    else:
        return 0


wirekeys = {}
for w in wires_cut:
    wire = w["wire"]
    if wire[0] not in wirekeys.keys():
        wirekeys[wire[0]] = 0
    wirekeys[wire[0]] += 1

    if wire[1] not in wirekeys.keys():
        wirekeys[wire[1]] = 0
    wirekeys[wire[1]] += 1
print("")
print(wirekeys.items())
start_points = [{"node": k, "count": n} for k, n in wirekeys.items()]
start_points.sort(key=lambda sp: sp["count"])

# print(wire_map)


def iterate(wire_map_in, wires):
    sums = []
    startcombinations = list(combinations(wires, 3))
    print(len(startcombinations))
    for wires_cut in startcombinations:
        wire_map = copy.deepcopy(wire_map_in)
        for start_node in list(wires_cut[0]):
            na = calculate_nodes(wire_map, start_node, wires_cut)
            if na != 0:
                sums.append(na)
        if len(sums) == 2:
            print(sums[0])
            print(sums[1])
            return sums[0] * sums[1]


# print(iterate(wire_map, list(wires)))
# print(sums[0] * sums[1])


def calculate_connections(wire_map, nodes):
    result = []
    for node in nodes:
        n = wire_map[node]
        for c in n["connections"]:
            if c not in nodes:
                wire = [node, c]
                wire.sort()
                result.append(wire)
    return result


def recursive_node_expansion(wire_map, nodes, current_node):
    con = calculate_connections(wire_map, nodes)
    print(len(con))
    if len(con) == 3 and len(nodes) > 1 and len(nodes) < len(wire_map.keys()) - 2:
        nodes_a = calculate_nodes(copy.deepcopy(wire_map), con[0][0], con)
        nodes_b = calculate_nodes(copy.deepcopy(wire_map), con[0][1], con)
        if (
            nodes_a > 0
            and nodes_b > 0
            and nodes_a < len(wire_map.keys())
            and nodes_b < len(wire_map.keys())
        ):
            print("hallooo")
            print(nodes)
            print(con)
            print(nodes_a)
            print(nodes_b)
            print(nodes_a * nodes_b)
            return True
    for node in current_node["connections"]:
        # print(node)
        if node not in nodes:
            nnodes = copy.deepcopy(nodes)
            nnodes.append(node)
            result = recursive_node_expansion(wire_map, nnodes, wire_map[node])
            if result:
                return True
    return False


start_node = list(wire_map.values())[0]


# recursive_node_expansion(wire_map, [start_node["name"]], start_node)
# print(
#     calculate_nodes(wire_map, "hfx", [("hfx", "pzl"), ("bvb", "cmg"), ("jqt", "nvd")])
# )
def contract(wire_map, wires, index):
    n_wires = []
    n_wire_map = {}
    wire = wires.pop(index)
    w = wire["wire"]
    a = w[0]
    b = w[1]
    node_a = wire_map[a]
    node_b = wire_map[b]
    connections = []
    for c in node_a["connections"]:
        if c != b:
            connections.append(c)
    for c in node_b["connections"]:
        if c != a:
            connections.append(c)
    old_nodes = []
    old_nodes.extend(node_a["old_nodes"])
    old_nodes.extend(node_b["old_nodes"])
    # old_nodes.append(a)
    # old_nodes.append(b)
    node = {
        "name": node_a["name"] + node_b["name"],
        "connections": connections,
        "wire_ids": [],
        "old_nodes": old_nodes,
    }
    for wire2 in wires:
        w2 = wire2["wire"]
        if (a == w2[0] and b == w2[1]) or (a == w2[1] and b == w2[0]):
            pass

        elif a == w2[0]:
            node["wire_ids"].append(wire2["name"])
            n_wire = {"name": wire2["name"]}
            nodes = [node["name"], w2[1]]
            nodes.sort()
            n_wire["wire"] = nodes
            n_wires.append(n_wire)
        elif a == w2[1]:
            node["wire_ids"].append(wire2["name"])
            n_wire = {"name": wire2["name"]}
            nodes = [node["name"], w2[0]]
            nodes.sort()
            n_wire["wire"] = nodes
            n_wires.append(n_wire)
        elif b == w2[0]:
            node["wire_ids"].append(wire2["name"])
            n_wire = {"name": wire2["name"]}
            nodes = [node["name"], w2[1]]
            nodes.sort()
            n_wire["wire"] = nodes
            n_wires.append(n_wire)
        elif b == w2[1]:
            node["wire_ids"].append(wire2["name"])
            n_wire = {"name": wire2["name"]}
            nodes = [node["name"], w2[0]]
            nodes.sort()
            n_wire["wire"] = nodes
            n_wires.append(n_wire)
        else:
            n_wires.append(wire2)

    for key in wire_map.keys():
        if key == a:
            continue
        if key == b:
            continue
        else:
            n_wire_map[key] = wire_map[key]

    n_wire_map[node["name"]] = node
    return n_wire_map, n_wires


def kargers(wire_map, wires):
    while True:
        current_wire_map = copy.deepcopy(wire_map)
        current_wires = copy.deepcopy(wires)
        # current_wire_map = wire_map
        # current_wires = wires
        while len(current_wire_map.keys()) > 2:
            index = random.randrange(len(current_wires))
            current_wire_map, current_wires = contract(
                current_wire_map, current_wires, index
            )
            # print("hello")
            # print(current_wire_map)
            # print(current_wires)
        if len(current_wires) == 3:
            valid = True
            for ckey in current_wire_map.keys():
                if len(current_wire_map[ckey]["old_nodes"]) == 1:
                    valid = False

            if not valid:
                continue
            print(len(current_wires))
            times = 1
            for ckey in current_wire_map.keys():
                print(len(current_wire_map[ckey]["old_nodes"]))
                times *= len(current_wire_map[ckey]["old_nodes"])
                print(current_wire_map[ckey]["old_nodes"])
            print(times)
            break


# print(wire_map)
print(dual_list)
result = kargers(wire_map, dual_list)
