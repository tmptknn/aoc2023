with open("input.txt") as f:
    filecontent = f.read()

mapping = {"red": 12, "green": 13, "blue": 14}

lines = filecontent.splitlines()

sum = 0
for game in lines:
    game_id_and_rounds = game.split(":")
    game_id = int(game_id_and_rounds[0][5:])

    is_valid = True
    rounds = game_id_and_rounds[1].split(";")
    for round in rounds:
        colors = round.split(",")
        for color in colors:
            numb_and_color = color.split(" ")
            numb = int(numb_and_color[1])
            color = numb_and_color[2]
            if mapping[color] < numb:
                is_valid = False

    if is_valid:
        sum += game_id

print(sum)
