with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()

sum = 0
for game in lines:
    game_id_and_rounds = game.split(":")
    game_id = int(game_id_and_rounds[0][5:])

    minimumset = {"red": 0, "green": 0, "blue": 0}
    rounds = game_id_and_rounds[1].split(";")
    for round in rounds:
        colors = round.split(",")
        for color in colors:
            numb_and_color = color.split(" ")
            numb = int(numb_and_color[1])
            color = numb_and_color[2]
            if minimumset[color] < numb:
                minimumset[color] = numb

    sum += minimumset["red"] * minimumset["green"] * minimumset["blue"]

print(sum)
