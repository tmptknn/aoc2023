import math
import numpy as np

with open("input.txt") as f:
    file_content = f.read().split("\n\n")

# print(file_content)


def interpret(symbol):
    if symbol == "#":
        return 1
    else:
        return 0


def get_array(lines):
    array = []
    for line in lines:
        array.append([interpret(symbol) for symbol in line])
    return array


def get_line_sums(lines):
    return [sum(line) for line in lines]


def scan(map, index, smudge):
    tmap = np.transpose(map)
    count = 0
    for j, line in enumerate(tmap):
        for i in range(len(line)):
            if index - i < 0 or index + 1 + i >= len(line):
                break
            if line[index - i] != line[index + 1 + i]:
                if index - i == smudge and count == 0:
                    count += 1
                    continue

                return False

    return True


def find_solution(linesums, map):
    lsums = len(linesums)
    result = []
    for i in range(lsums):
        found = True
        smudge = -1
        for j in range(lsums):
            j0 = i - j
            j1 = i + j + 1
            if j0 < 0 or j1 > lsums - 1:
                break

            if linesums[j0] != linesums[j1]:
                if smudge == -1:
                    smudge = j0
                    continue
                found = False
                break
        if found and i >= 0 and i < lsums - 1:
            if smudge != -1 and scan(map, i, smudge):
                result.append(i + 1)
            # return i + 1

    return result


def printmap(map):
    for line in map:
        pline = ""
        for c in line:
            if c == 1:
                pline += "1"
            else:
                pline += "0"
        print(pline)


total = 0
for i, maplines in enumerate(file_content):
    map = get_array(maplines.split("\n"))

    # print(map)
    vertical = find_solution(get_line_sums(np.transpose(map)), np.transpose(map))
    horizontal = find_solution(get_line_sums(map), map)
    # print(vertical)
    # print(horizontal)
    # if not (len(vertical) == 0 or len(horizontal) == 0):
    if not (
        (len(vertical) == 0 or len(horizontal) == 0)
        and (len(horizontal) == 1 or len(vertical) == 1)
    ):
        print("map " + str(i))
        print(horizontal)
        print(vertical)
        printmap(map)

    vsum = sum(vertical)
    hsum = sum(horizontal)

    total += vsum + 100 * hsum
    # total += vertical + 100 * horizontal

print(total)
