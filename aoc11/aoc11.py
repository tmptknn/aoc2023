import math

with open("input.txt") as f:
    file_content = f.read().split("\n")

galaxy = []
for row in file_content:
    galaxy.append([{"type": s} for s in row])

# print(galaxy)


def print_galaxy(galaxy):
    for row in galaxy:
        pline = ""
        for space in row:
            if "galaxy_number" in space.keys():
                pline += str(space["galaxy_number"] % 10)
            else:
                pline += space["type"]
        # print("".join([space["type"] for space in row]))
        print(pline)


print_galaxy(galaxy)

print("")
for i in range(len(galaxy[0]) - 1, -1, -1):
    empty = True
    for j in range(len(galaxy)):
        space = galaxy[j][i]
        if space["type"] == "#":
            empty = False

    if empty:
        for j in range(len(galaxy)):
            galaxy[j][i] = {"type": "+"}

print_galaxy(galaxy)
print(" ")
for i in range(len(galaxy) - 1, -1, -1):
    empty = True
    for j in range(len(galaxy[0])):
        space = galaxy[i][j]
        if space["type"] == "#":
            empty = False

    if empty:
        galaxy[i] = [{"type": "+"} for space in galaxy[0]]

print_galaxy(galaxy)
print("")
index = 0
galaxies = []
for j, row in enumerate(galaxy):
    for i, space in enumerate(row):
        if space["type"] == "#":
            galaxies.append({"x": i, "y": j, "number": index, "name": index + 1})
            index += 1
            space["galaxy_number"] = index


print_galaxy(galaxy)

# print(galaxies)
pairs = []

for i in range(len(galaxies)):
    for j in range(i + 1, len(galaxies)):
        ax = min(galaxies[i]["x"], galaxies[j]["x"])
        bx = max(galaxies[i]["x"], galaxies[j]["x"])
        ay = min(galaxies[i]["y"], galaxies[j]["y"])
        by = max(galaxies[i]["y"], galaxies[j]["y"])
        warpsx = 0
        for k in range(bx - ax):
            if galaxy[0][ax + k]["type"] == "+":
                warpsx += 1

        warpsy = 0
        for k in range(by - ay):
            if galaxy[ay + k][0]["type"] == "+":
                warpsy += 1
        pairs.append(
            {"A": galaxies[i], "B": galaxies[j], "warpsx": warpsx, "warpsy": warpsy}
        )

# print(pairs)
sum = 0
multiplier = 1000000
for pair in pairs:
    xlen = (
        abs(pair["A"]["x"] - pair["B"]["x"])
        - pair["warpsx"]
        + pair["warpsx"] * multiplier
    )
    ylen = (
        abs(pair["A"]["y"] - pair["B"]["y"])
        - pair["warpsy"]
        + pair["warpsy"] * multiplier
    )

    distance = xlen + ylen
    pair["distance"] = distance
    sum += distance

# print(pairs)
print(sum)
