with open("input.txt") as f:
    lines = f.readlines()

digits = "0123456789"

numbers = []
symbols = []

for i, line in enumerate(lines):
    start = -1
    for j, letter in enumerate(line):
        if letter in digits:
            if start == -1:
                start = j
        else:
            if start != -1:
                numbers.append(
                    {
                        "line": i,
                        "start": start,
                        "end": j - 1,
                        "number": int(line[start:j]),
                    }
                )
                start = -1

            if letter != "\n" and letter != ".":
                symbols.append({"symbol": line[j], "x": j, "y": i})

sum = 0
for symbol in symbols:
    if symbol["symbol"] == "*":
        numbers_adjacent = []
        for number in numbers:
            if (
                number["line"] >= symbol["y"] - 1
                and number["line"] <= symbol["y"] + 1
                and number["start"] <= symbol["x"] + 1
                and number["end"] >= symbol["x"] - 1
            ):
                numbers_adjacent.append(number)

        if len(numbers_adjacent) == 2:
            sum += numbers_adjacent[0]["number"] * numbers_adjacent[1]["number"]
print(sum)
