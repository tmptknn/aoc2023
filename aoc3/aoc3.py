with open("input.txt") as f:
    lines = f.readlines()

digits = "0123456789"

numbers = []
symbols = []

for i, line in enumerate(lines):
    start = -1
    for j, letter in enumerate(line):
        if letter in digits:
            if start == -1:
                start = j
        else:
            if start != -1:
                numbers.append(
                    {
                        "line": i,
                        "start": start,
                        "end": j - 1,
                        "number": int(line[start:j]),
                    }
                )
                start = -1

            if letter != "\n" and letter != ".":
                symbols.append({"symbol": line[j], "x": j, "y": i})

sum = 0
for number in numbers:
    add_number = False
    for symbol in symbols:
        if (
            number["line"] >= symbol["y"] - 1
            and number["line"] <= symbol["y"] + 1
            and number["start"] <= symbol["x"] + 1
            and number["end"] >= symbol["x"] - 1
        ):
            add_number = True

    if add_number:
        sum += number["number"]
print(sum)
