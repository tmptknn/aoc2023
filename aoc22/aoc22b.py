
# Import libraries
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import json
import copy

with open("input.txt") as f:
    lines = f.read().split("\n")

bricks = []
bricks_map= {}
alpha = 0.5
for i,line in enumerate(lines):
    ends=line.split("~")
    start=ends[0].split(",")
    end=ends[1].split(",")
    brick = {"id":i+1,"min":[int(s) for s in start],"max":[int(e) for e in end], "supported_by":[], "supports":[]}
    bricks.append(brick)
    bricks_map[i+1] = brick

#print(bricks)
#print(bricks_map)
# Create axis
#axes = [5, 5, 5]
minr = [100,100,100]
maxr = [-1,-1,-1]
for brick in bricks:
    for i in range(3):
        minr[i] =min(brick["min"][i],minr[i])
        maxr[i] = max(brick["max"][i]+1,maxr[i])

for brick in bricks:

    minrng = brick["min"]
    maxrng = brick["max"]
    brick["color"]=[minrng[0]/maxr[0],minrng[1]/maxr[1],minrng[2]/maxr[2],alpha]

# print(maxr)
# Create Data
# Control colour


bricks.sort(key = lambda b: b["min"][2])


def drop_bricks(bricks, bricks_map):
    tallest ={}
    for i, brick in enumerate(bricks):
        print(str(i)+" out of "+str(len(bricks)))
        while True:
            hits = False
            minrng=[brick["min"][0],brick["min"][1],brick["min"][2]-1]
            maxrng=[brick["max"][0],brick["max"][1],brick["max"][2]-1]
            #for j in range( i-1,-1,-1):
            for x in range(minrng[0],maxrng[0]+1):
                for y in range(minrng[1],maxrng[1]+1):
                    if (x,y) in tallest.keys():
                        j = tallest[(x,y)]
                        support_brick = bricks_map[j]
                        sminrng = support_brick["min"]
                        smaxrng = support_brick["max"]
                        overlaps = True
                        for k in range(3):
                            maxmin = max(minrng[k],sminrng[k])
                            minmax = min(maxrng[k],smaxrng[k])
                            if maxmin > minmax:                        
                                overlaps= False
                        if overlaps:
                            
                            hits = True
                            brick["supported_by"].append(support_brick["id"])
                            support_brick["supports"].append(brick["id"])

            if not hits:
                if brick["min"][2] <= 0:
                    brick["supported_by"].append(-1)
                    for x in range(minrng[0],maxrng[0]+1):
                        for y in range(minrng[1], maxrng[1]+1):
                            tallest[(x,y)]=brick["id"]
                    break
                if brick["min"][2]>0:
                    brick["min"][2]-=1
                    brick["max"][2]-=1
                
            else:
                for x in range(minrng[0],maxrng[0]+1):
                    for y in range(minrng[1], maxrng[1]+1):
                        tallest[(x,y)]=brick["id"]
                break
                #brick["min"][2]+=1
                #brick["max"][2]+=1
                #break        
                 
            


def disintegrate_bricks(bricks_in):
    s=0
    dropped = 0
    for d, disintegrated_brick in enumerate(bricks_in):
        tallest ={}
        print(str(d)+" out of "+str(len(bricks_in))+" sum "+str(s))
        bricks = copy.deepcopy(bricks_in)
        bricks_map ={ b["id"]:b for b in bricks}
        bricks.pop(d)
        for i in range(len(bricks)):
            brick=bricks[i]
            
            
            while True:
                hits = False
                minrng=[brick["min"][0],brick["min"][1],brick["min"][2]-1]
                maxrng=[brick["max"][0],brick["max"][1],brick["max"][2]-1]
                #for j in range( i-1,-1,-1):
                for x in range(minrng[0],maxrng[0]+1):
                    for y in range(minrng[1],maxrng[1]+1):
                        if (x,y) in tallest.keys():
                            j = tallest[(x,y)]

                            support_brick = bricks_map[j]
                            sminrng = support_brick["min"]
                            smaxrng = support_brick["max"]
                            overlaps = True
                            for k in range(3):
                                maxmin = max(minrng[k],sminrng[k])
                                minmax = min(maxrng[k],smaxrng[k])
                                if maxmin > minmax:                        
                                    overlaps= False
                            if overlaps:
                                
                                hits = True
                                #brick["supported_by"].append(support_brick["id"])
                                #support_brick["supports"].append(brick["id"])

                if not hits:
                    if brick["min"][2] <= 0:
                        brick["supported_by"].append(-1)
                        for x in range(minrng[0],maxrng[0]+1):
                            for y in range(minrng[1], maxrng[1]+1):
                                tallest[(x,y)]=brick["id"]
                        break
                    if brick["min"][2]>0:
                        brick["min"][2]-=1
                        brick["max"][2]-=1
                        s+=1
                        break
                else:
                    for x in range(minrng[0],maxrng[0]+1):
                        for y in range(minrng[1], maxrng[1]+1):
                            tallest[(x,y)]=brick["id"]
                    break

    return s

drop_bricks(bricks, bricks_map)
#with open("temp_file.json") as infile:
#    bricks = json.loads(infile.read())

bricks.sort(key = lambda b: b["min"][2])
print(disintegrate_bricks(bricks))

# with open("temp_file.json", "w") as open_file:
#     open_file.write(json.dumps(bricks))
# bricks_map = {brick["id"]:brick for brick in bricks}
#print(bricks)


# sum=0
# for i in range(len(bricks)):
#     print("drop test "+str(i)+" of "+str(len(bricks))+" "+str(sum))
#     temp_bricks = copy.deepcopy(bricks)
#     print("deepcopy done")
#     disintegrate_brick = temp_bricks.pop(i)
#     if not drop_bricks(temp_bricks,i, True):
#         sum+=1


# print("disintegratable bricks count "+str(sum))

minr = [100,100,100]
maxr = [-1,-1,-1]
for brick in bricks:
    for i in range(3):
        minr[i] =min(brick["min"][i]-1,minr[i])
        maxr[i] = max(brick["max"][i]+2,maxr[i])
# print(bricks[0])
#print(json.dumps(bricks, indent=2))
# Control Transparency
colors = np.empty(maxr + [4], dtype=np.float32)
data = np.zeros(maxr, dtype=np.int32)
for brick in bricks:
    minrng = brick["min"]
    maxrng = brick["max"]
    data[minrng[0]:maxrng[0]+1,minrng[1]:maxrng[1]+1,minrng[2]:maxrng[2]+1]=brick["id"]
    colors[minrng[0]:maxrng[0]+1,minrng[1]:maxrng[1]+1,minrng[2]:maxrng[2]+1] = brick["color"] 

#colors[:] = [1, 0, 0, alpha]  # red
 
# Plot figure
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set(xlim=(minr[0]-1, maxr[0]+1), ylim=(minr[1]-1, maxr[1]+2), zlim=(minr[2]-1,maxr[2]+1))
def press(event):
    print('press', event.key)
    if event.key == "escape":
        plt.close()
# Voxels is used to customizations of the
# sizes, positions and colors.
ax.voxels(data, facecolors=colors)
ax.set_aspect('equal')
plt.show()
#plt.waitforbuttonpress()