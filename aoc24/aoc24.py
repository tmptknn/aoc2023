import math

with open("input.txt") as f:
    lines = f.read().split("\n")


hailstorms = []

for line in lines:
    pos_and_speed = line.split(" @ ")
    pos = pos_and_speed[0].split(", ")
    speed = pos_and_speed[1].split(",")
    hailstorms.append(([int(p) for p in pos], [int(s) for s in speed]))

print(hailstorms)


def calc_equ(h):
    p0 = [h[0][0], h[0][1]]
    d0 = [h[1][0], h[1][1]]
    multiplier = math.gcd(*d0)

    x1 = p0[0]
    y1 = p0[1]
    x2 = p0[0] + d0[0]
    y2 = p0[1] + d0[1]

    x3 = 0
    y3 = 0
    x4 = 0
    y4 = 1

    nominator = -(x1 - x2)

    if nominator == 0:
        py = 0
    else:
        py = -(x1 * y2 - y1 * x2) / nominator

    p0[0] = 0
    p0[1] = py
    d0[0] = d0[0] / multiplier
    d0[1] = d0[1] / multiplier
    return p0, d0, multiplier


def cross(h0, h1):
    p0 = [h0[0][0], h0[0][1]]
    d0 = [h0[1][0], h0[1][1]]

    x1 = p0[0]
    y1 = p0[1]
    x2 = p0[0] + d0[0]
    y2 = p0[1] + d0[1]

    p1 = [h1[0][0], h1[0][1]]
    d1 = [h1[1][0], h1[1][1]]

    x3 = p1[0]
    y3 = p1[1]
    x4 = p1[0] + d1[0]
    y4 = p1[1] + d1[1]

    nominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    if nominator != 0:
        px = (
            (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)
        ) / nominator
        py = (
            (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)
        ) / nominator

        return px, py

    assert False
    return x1, y1


def cross_product(v0, v1):
    return v0[0] * v1[1] - v0[1] * v1[0]


def dot_product(v0, v1):
    return v0[0] * v1[0] + v0[1] * v1[1]


def parallel(h0, h1):
    p0 = [h0[0][0], h0[0][1]]
    d0 = [h0[1][0], h0[1][1]]

    x1 = p0[0]
    y1 = p0[1]
    x2 = p0[0] + d0[0]
    y2 = p0[1] + d0[1]

    p1 = [h1[0][0], h1[0][1]]
    d1 = [h1[1][0], h1[1][1]]

    x3 = p1[0]
    y3 = p1[1]
    x4 = p1[0] + d1[0]
    y4 = p1[1] + d1[1]

    nominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    return nominator == 0


def past(h0, h1):
    x, y = cross(h0, h1)
    v0a = [x - h0[0][0], y - h0[0][1]]
    v0b = [h0[1][0], h0[1][1]]
    dot0 = dot_product(v0a, v0b)

    v1a = [x - h1[0][0], y - h1[0][1]]
    v1b = [h1[1][0], h1[1][1]]
    dot1 = dot_product(v1a, v1b)
    return dot0 < 0 or dot1 < 0


def inside(x, y, xmin, xmax, ymin, ymax):
    if x < xmin or x > xmax:
        return False
    if y < ymin or y > ymax:
        return False
    return True


def eval(hailstorms, xmin, xmax, ymin, ymax):
    suma = 0
    for i, h0 in enumerate(hailstorms):
        e0 = calc_equ(h0)
        for j in range(i + 1, len(hailstorms)):
            # print(i)
            # print(j)
            h1 = hailstorms[j]
            e1 = calc_equ(h1)

            if e0[1][0] == e1[1][0] and e0[1][1] == e1[1][1]:
                # print(e0)
                # print(e1)
                if e0[0][0] == e1[0][0] and e0[0][1] == e1[0][1] and e0[2] == e1[2]:
                    print("Are same")
                else:
                    print("Are parallel and do not meet")

            elif h0[1][0] == -h1[1][0] and h0[1][1] == -h1[1][1]:
                if h0[0][0] == h1[0][0] and h0[0][1] == h1[0][1]:
                    print("Are opposite")
                else:
                    print("Are opposite and do not meet")
                assert False
            elif parallel(h0, h1):
                print("special case")
            else:
                x, y = cross(h0, h1)
                print("At " + str(x) + " " + str(y))

                is_past = past(h0, h1)
                if not is_past:
                    is_inside = inside(x, y, xmin, xmax, ymin, ymax)
                    if is_inside:
                        print("inside test area")
                    else:
                        print("outside test area")

                    if is_inside and not is_past:
                        suma += 1
                    print("")
    return suma


print(
    eval(hailstorms, 200000000000000, 400000000000000, 200000000000000, 400000000000000)
)
