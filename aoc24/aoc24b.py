import math
from itertools import combinations
import numpy as np

with open("input.txt") as f:
    lines = f.read().split("\n")


hailstorms = []

for line in lines:
    pos_and_speed = line.split(" @ ")
    pos = pos_and_speed[0].split(", ")
    speed = pos_and_speed[1].split(",")
    hailstorms.append(([int(p) for p in pos], [int(s) for s in speed]))

print(hailstorms)


def throw_shit(hailstorms):  # mostly borrowed
    (x0, y0, z0), (vx0, vy0, vz0) = hailstorms[4]
    (x1, y1, z1), (vx1, vy1, vz1) = hailstorms[25]
    (x2, y2, z2), (vx2, vy2, vz2) = hailstorms[37]

    equation_matrix = np.zeros((6, 6), dtype=np.float64)
    vector = np.zeros(6, dtype=np.float64)

    equation_matrix[0, 1] = vz0 - vz1
    equation_matrix[0, 2] = vy1 - vy0
    equation_matrix[0, 4] = z1 - z0
    equation_matrix[0, 5] = y0 - y1

    equation_matrix[1, 0] = vz1 - vz0
    equation_matrix[1, 2] = vx0 - vx1
    equation_matrix[1, 3] = z0 - z1
    equation_matrix[1, 5] = x1 - x0

    equation_matrix[2, 0] = vy0 - vy1
    equation_matrix[2, 1] = vx1 - vx0
    equation_matrix[2, 3] = y1 - y0
    equation_matrix[2, 4] = x0 - x1

    equation_matrix[3, 1] = vz0 - vz2
    equation_matrix[3, 2] = vy2 - vy0
    equation_matrix[3, 4] = z2 - z0
    equation_matrix[3, 5] = y0 - y2

    equation_matrix[4, 0] = vz2 - vz0
    equation_matrix[4, 2] = vx0 - vx2
    equation_matrix[4, 3] = z0 - z2
    equation_matrix[4, 5] = x2 - x0

    equation_matrix[5, 0] = vy0 - vy2
    equation_matrix[5, 1] = vx2 - vx0
    equation_matrix[5, 3] = y2 - y0
    equation_matrix[5, 4] = x0 - x2

    indepx0 = y0 * vz0 - vy0 * z0
    indepx1 = y1 * vz1 - vy1 * z1
    indepx2 = y2 * vz2 - vy2 * z2

    indepy0 = z0 * vx0 - vz0 * x0
    indepy1 = z1 * vx1 - vz1 * x1
    indepy2 = z2 * vx2 - vz2 * x2

    indepz0 = x0 * vy0 - vx0 * y0
    indepz1 = x1 * vy1 - vx1 * y1
    indepz2 = x2 * vy2 - vx2 * y2

    vector[0] = indepx0 - indepx1
    vector[1] = indepy0 - indepy1
    vector[2] = indepz0 - indepz1
    vector[3] = indepx0 - indepx2
    vector[4] = indepy0 - indepy2
    vector[5] = indepz0 - indepz2

    result = np.linalg.solve(equation_matrix, vector)
    coordinates = result[:3]
    return int(coordinates[0] + coordinates[1] + coordinates[2])


print(throw_shit(hailstorms))
