with open("input.txt") as f:
    file_content = f.read().split("\n")


def hex_to_rgb(hexa):
    return tuple(int(hexa[i + 1 : i + 3], 16) for i in (0, 2, 4))


def direction_to_vec(direction):
    if direction == "U":
        return [0, -1]
    if direction == "D":
        return [0, 1]
    if direction == "R":
        return [1, 0]
    if direction == "L":
        return [-1, 0]
    assert False


dig_plan = []
for row in file_content:
    split_row = row.split(" ")
    direction = split_row[0]
    distance = split_row[1]
    color = split_row[2][1:-1]
    dig_plan.append(
        {
            "direction": direction_to_vec(direction),
            "distance": int(distance),
            "color": hex_to_rgb(color),
        }
    )

print(dig_plan)


def print_ground(ground):
    for row in ground:
        pline = ""
        for pos in row:
            pline += pos["symbol"]
        print(pline)


def mark_interior(ground):
    for row in ground:
        inside = False
        previous = None
        for pos in row:
            current = pos["symbol"]
            if pos["symbol"] == "." and inside:
                pos["symbol"] = "#"
                pos["interior"] = True
            elif (
                pos["symbol"] == "#"
                and abs(pos["direction"][1]) == 1
                and pos["direction"][1] != previous
            ):
                inside = not inside
                pos["interior"] = False

            if current == "#":
                previous = pos["direction"][1]


def calc_area(ground):
    sum = 0
    for row in ground:
        for pos in row:
            if pos["symbol"] == "#":
                sum += 1
    return sum


def insert_row(ground, index):
    l = len(ground[0])
    line = []
    for i in range(l):
        line.append({"symbol": "."})
    if index < len(ground) and index >= 0:
        ground.insert(index, line)
    elif index == len(ground):
        ground.append(line)
    else:
        assert False


def insert_column(ground, index):
    l = len(ground[0])
    for line in ground:
        pos = {"symbol": "."}
        if index < l and index >= 0:
            line.insert(index, pos)
        elif index == l:
            line.append(pos)
        else:
            assert False


ground = [[{"symbol": "."}]]
# print_ground(ground)
# insert_row(ground, 1)
# print_ground(ground)
# insert_column(ground, 0)
# print_ground(ground)
# insert_column(ground, 2)
# print_ground(ground)


def apply_dig_plan(dig_plan, ground):
    current = [0, 0]
    for index, plan in enumerate(dig_plan):
        direction = plan["direction"]
        if "direction" in ground[current[1]][current[0]]:
            ground[current[1]][current[0]]["direction"][0] += direction[0]
            ground[current[1]][current[0]]["direction"][1] += direction[1]
        for i in range(1, plan["distance"] + 1):
            d = [current[0] + i * direction[0], current[1] + i * direction[1]]
            if abs(direction[0]) > 0:
                if d[0] < 0:
                    insert_column(ground, 0)
                    d[0] = 0
                elif d[0] >= len(ground[0]):
                    insert_column(ground, len(ground[0]))
            else:
                if d[1] < 0:
                    insert_row(ground, 0)
                    d[1] = 0
                elif d[1] >= len(ground):
                    insert_row(ground, len(ground))

            # print(d[0])
            # print(d[1])
            ground[d[1]][d[0]]["symbol"] = "#"
            ground[d[1]][d[0]]["color"] = plan["color"]
            ground[d[1]][d[0]]["direction"] = plan["direction"]

        current[0] = d[0]
        current[1] = d[1]
        # print_ground(ground)
        print(" ")


apply_dig_plan(dig_plan, ground)
print_ground(ground)
mark_interior(ground)
print_ground(ground)
print(calc_area(ground))
