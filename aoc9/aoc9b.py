from datetime import datetime

with open("input.txt") as f:
    file_content = f.readlines()

histories = []
for line in file_content:
    histories.append([[int(numb) for numb in line.split()]])

# print(histories)
sum = 0
for history in histories:
    current_line = 0
    while True:
        all_zeroes = True
        line = history[current_line]
        len_line = len(line)
        add_line = []
        for i in range(0, len_line - 1):
            value = line[i + 1] - line[i]
            add_line.append(value)
            if value != 0:
                all_zeroes = False
        history.append(add_line)
        current_line += 1
        if all_zeroes:
            break

    line = history[current_line]
    line.insert(0, line[0])
    current_line -= 1
    last_value = 0

    while current_line >= 0:
        line = history[current_line]
        print(last_value)
        last_value = line[0] - last_value
        line.insert(0, last_value)
        current_line -= 1
    sum += last_value

print(histories)
print(sum)
