import math
import numpy as np

with open("input.txt") as f:
    file_content = f.read()


def interpret(symbol):
    if symbol == "O":
        return 1
    if symbol == "#":
        return 2
    else:
        return 0


def get_array(lines):
    array = []
    for line in lines:
        array.append([{"symbol":symbol, "value":interpret(symbol)} for symbol in line])
    return array


def print_map(map):
    for line in map:
        pline=""
        for pos in line:
            pline+=pos["symbol"]
        print(pline)


def roll_north(map):
    for i in range(len(map[0])):
        rocks = 0
        for j in range(len(map)-1,-1,-1):
            symbol = map[j][i]["symbol"]
            if symbol=="#":
                rocks = 0
            if symbol==".":
                if rocks>0:
                    map[j][i]["symbol"] = "O"
                    map[j+rocks][i]["symbol"] = "."

            if symbol=="O":
                map[j+rocks][i]["symbol"]="O"
                rocks+=1

                

def calc_sum(map):
    sum=0
    for i,line in enumerate(map):
        line_weight = 0
        rock_weight = len(map)-i
        for pos in line:
            if pos["symbol"] == "O":
                line_weight+=rock_weight

        sum+=line_weight

    return sum

map = get_array(file_content.split("\n"))

print_map(map)
print("")
roll_north(map)
print_map(map)
print(calc_sum(map))