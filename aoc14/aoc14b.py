import math
import numpy as np

with open("input.txt") as f:
    file_content = f.read()


def interpret(symbol):
    if symbol == "O":
        return 1
    if symbol == "#":
        return 2
    else:
        return 0


def get_array(lines):
    array = []
    for line in lines:
        array.append([{"symbol":symbol, "value":interpret(symbol)} for symbol in line])
    return array


def print_map(map):
    for line in map:
        pline=""
        for pos in line:
            pline+=pos["symbol"]
        print(pline)


def roll_north(map):
    for i in range(len(map[0])):
        rocks = 0
        for j in range(len(map)-1,-1,-1):
            symbol = map[j][i]["symbol"]
            if symbol=="#":
                rocks = 0
            if symbol==".":
                if rocks>0:
                    map[j][i]["symbol"] = "O"
                    map[j+rocks][i]["symbol"] = "."

            if symbol=="O":
                map[j+rocks][i]["symbol"]="O"
                rocks+=1


def roll_south(map):
    for i in range(len(map[0])):
        rocks = 0
        for j in range(len(map)):
            symbol = map[j][i]["symbol"]
            if symbol=="#":
                rocks = 0
            if symbol==".":
                if rocks>0:
                    map[j][i]["symbol"] = "O"
                    map[j-rocks][i]["symbol"] = "."

            if symbol=="O":
                map[j-rocks][i]["symbol"]="O"
                rocks+=1

def roll_west(map):
    for j in range(len(map[0])):
        rocks = 0
        for i in range(len(map)-1,-1,-1):
            symbol = map[j][i]["symbol"]
            if symbol=="#":
                rocks = 0
            if symbol==".":
                if rocks>0:
                    map[j][i]["symbol"] = "O"
                    map[j][i+rocks]["symbol"] = "."

            if symbol=="O":
                map[j][i+rocks]["symbol"]="O"
                rocks+=1

def roll_east(map):
    for j in range(len(map)):
    
        rocks = 0
        for i in range(len(map[0])):
            symbol = map[j][i]["symbol"]
            if symbol=="#":
                rocks = 0
            if symbol==".":
                if rocks>0:
                    map[j][i]["symbol"] = "O"
                    map[j][i-rocks]["symbol"] = "."

            if symbol=="O":
                map[j][i-rocks]["symbol"]="O"
                rocks+=1

def calc_sum(map):
    sum=0
    for i,line in enumerate(map):
        line_weight = 0
        rock_weight = len(map)-i
        for pos in line:
            if pos["symbol"] == "O":
                line_weight+=rock_weight

        sum+=line_weight

    return sum

def calc_check_sum(map):
    sum=0
    for i,line in enumerate(map):
        line_weight = 0
        rock_weight = (i+1)*(len(map[0])+1)
        for j,pos in enumerate(line):
            if pos["symbol"] == "O":
                line_weight+=rock_weight+(j+1)

        sum+=line_weight

    return sum

def cycle(map):
    roll_north(map)
    roll_west(map)
    roll_south(map)
    roll_east(map)


def cycle_until_repeats(map):
    cycles = 0
    checksums = []
    while True:
        cycle(map)
        
        checksum = calc_check_sum(map)
        for i in range(len(checksums)):
            if checksum == checksums[i]:
                return cycles,i
        cycles +=1
        checksums.append(checksum)


map = get_array(file_content.split("\n"))

#print_map(map)
#print("")
#roll_north(map)
#print_map(map)
#print(calc_sum(map))
# cycle(map)
# print("cycle 1")
# print_map(map)
# cycle(map)
# print("cycle 2")
# print_map(map)
# cycle(map)
# print("cycle 3")
# print_map(map)
cycles_in, offset =cycle_until_repeats(map)
cycle_length = cycles_in-offset
print("starts to cycle in "+str(cycles_in)+ " cycle length "+ str(cycle_length))
cycle_phase=(1000000000-offset)%cycle_length
map = get_array(file_content.split("\n"))
for i in range(offset+cycle_phase):
    cycle(map)

print(calc_sum(map))