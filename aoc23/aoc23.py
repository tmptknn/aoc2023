import copy
import json

with open("input.txt") as f:
    lines = f.read().split("\n")


water_map = []
start = None
for y, line in enumerate(lines):
    water_line = []
    for x, pos in enumerate(line):
        spot = {"symbol": pos, "x": x, "y": y, "visited": False}
        if pos == ".":
            if y == 0:
                start = (y, x)
            if y == len(lines) - 1:
                end = (y, x)
        water_line.append(spot)
    water_map.append(water_line)

print(water_map)
print(start)
print(end)

steps = [(0, -1), (1, 0), (0, 1), (-1, 0)]


def print_routes(knots, routes):
    knot_routes = [0 for _ in knots]
    for route in routes:
        a = route["A"]
        b = route["B"]
        ax = -1
        bx = -1
        for i, knot in enumerate(knots):
            kpos = knot["pos"]
            if a[0] == kpos[0] and a[1] == kpos[1]:
                ax = i
            if b[0] == kpos[0] and b[1] == kpos[1]:
                bx = i
        print(
            str(ax)
            + " "
            + str(bx)
            + "  "
            + str(route["distance"])
            + " "
            + str(route["direction"])
        )
        knot_routes[ax] += 1
        knot_routes[bx] += 1
    print(knot_routes)


def print_cross(knots):
    for y, line in enumerate(water_map):
        pline = ""
        for x, p in enumerate(line):
            ch = p["symbol"]
            for i, knot in enumerate(knots):
                if x == knot["pos"][1] and y == knot["pos"][0]:
                    if ch != ".":
                        print(str(x) + " " + str(y) + " " + str(i))
                        assert False

                    ch = str(i)

            pline += ch
        print(pline)


def valid_pos(pos):
    if pos[0] > len(water_map) - 1 or pos[0] < 0:
        return False
    if pos[1] > len(water_map[0]) - 1 or pos[1] < 0:
        return False
    return True


def calculate_routes(current, last_pos):
    nroutes = []
    for s in steps:
        if current[0] + s[0] == last_pos[0] and current[1] + s[1] == last_pos[1]:
            continue
        if not valid_pos((current[0] + s[0], current[1] + s[1])):
            continue
        position = (current[0] + s[0], current[1] + s[1])
        wp = water_map[position[0]][position[1]]
        if wp["symbol"] != "#":
            nroutes.append(position)
    return nroutes


def check_current_route_direction(current_route_direction, position, current, wp):
    if wp["symbol"] != ".":
        pot_op = True
        s = (position[0] - current[0], position[1] - current[1])
        if s[1] == -1 and wp["symbol"] == "<":
            pot_op = False
        elif s[0] == 1 and wp["symbol"] == "v":
            pot_op = False
        elif s[1] == 1 and wp["symbol"] == ">":
            pot_op = False
        elif s[0] == -1 and wp["symbol"] == "^":
            pot_op = False

        if pot_op and current_route_direction == 1:
            print("route not valid hill")
            assert False
        elif not pot_op and current_route_direction == -1:
            print("route not valid hole")
            assert False
        current_route_direction = 1
        if pot_op:
            current_route_direction = -1
    return current_route_direction


def make_route(
    trail_map,
    trail_mapping,
    current,
    current_route_direction,
    routes,
    last_knot,
    distance,
):
    if current in trail_mapping.keys():
        this_knot = trail_mapping[current]
    else:
        this_knot = {"pos": current, "connections": []}
        trail_map.append(this_knot)
        trail_mapping[current] = this_knot

    if current_route_direction == -1:
        route = {
            "A": current,
            "B": last_knot["pos"],
            "distance": distance,
            "direction": -1,
        }
        routes.append(route)
        this_knot["connections"].append({"other_end": last_knot["pos"], "route": route})
    elif current_route_direction == 1:
        route = {
            "A": last_knot["pos"],
            "B": current,
            "distance": distance,
            "direction": 1,
        }
        routes.append(route)
        last_knot["connections"].append({"other_end": this_knot["pos"], "route": route})
    elif current_route_direction == 0:
        route = {
            "A": last_knot["pos"],
            "B": current,
            "distance": distance,
            "direction": 0,
        }
        routes.append(route)
        last_knot["connections"].append({"other_end": this_knot["pos"], "route": route})
        routes.append(route)
        this_knot["connections"].append({"other_end": last_knot["pos"], "route": route})
    else:
        assert False
    return this_knot


def handle_knot(
    trail_map,
    trail_mapping,
    current,
    last_pos,
    positions,
    current_route_direction,
    distance,
    last_knot,
    routes,
):
    this_knot = make_route(
        trail_map,
        trail_mapping,
        current,
        current_route_direction,
        routes,
        last_knot,
        distance,
    )
    # print(trail_mapping)
    for position in positions:
        if position[0] == last_pos[0] and position[1] == last_pos[1]:
            continue
        if not valid_pos((position[0], position[1])):
            continue
        wp = water_map[position[0]][position[1]]
        if wp["symbol"] != "#" and last_pos != position and wp["visited"] == False:
            rec_make_map(
                water_map,
                trail_map,
                trail_mapping,
                position,
                current,
                this_knot,
                end,
                routes,
            )
            continue
        elif wp["symbol"] != "#":
            print("WTF")


def rec_make_map(
    water_map_in, trail_map, trail_mapping, current, last_pos, last_knot, end, routes
):
    # water_map = copy.deepcopy(water_map_in)
    water_map = water_map_in
    distance = 1
    current_route_direction = 0
    water_map[current[0]][current[1]]["visited"] = True
    while True:
        nroutes = calculate_routes(current, last_pos)
        if len(nroutes) == 0:
            if current[0] == end[0] and current[1] == end[1]:
                if end not in trail_mapping.keys():
                    route = {
                        "A": last_knot["pos"],
                        "B": current,
                        "distance": distance,
                        "direction": current_route_direction,
                    }
                    routes.append(route)
                    last_knot["connections"].append({"other_end": end, "route": route})
                    this_knot = {"pos": end, "connections": []}
                    trail_map.append(this_knot)
                    trail_mapping[end] = this_knot
            return

        if len(nroutes) == 1:
            for position in nroutes:
                if position in trail_mapping.keys():
                    this_knot = make_route(
                        trail_map,
                        trail_mapping,
                        position,
                        current_route_direction,
                        routes,
                        last_knot,
                        distance + 1,
                    )
                    return
                wp = water_map[position[0]][position[1]]
                if wp["visited"]:
                    break
                wp["visited"] = True
                current_route_direction = check_current_route_direction(
                    current_route_direction, position, current, wp
                )
                distance += 1
                last_pos = current
                current = position
            continue
        if len(nroutes) > 1:
            water_map[current[0]][current[1]]["visited"] = True
            handle_knot(
                trail_map=trail_map,
                trail_mapping=trail_mapping,
                current=current,
                last_pos=last_pos,
                positions=nroutes,
                current_route_direction=current_route_direction,
                distance=distance,
                last_knot=last_knot,
                routes=routes,
            )
        # wp["visited"] = True
        return


def print_visited(water_map):
    for y, line in enumerate(water_map):
        pline = ""
        for x, p in enumerate(line):
            ch = p["symbol"]
            if p["visited"]:
                ch = "X"
            pline += ch
        print(pline)


def make_map(water_map, start, end):
    # copy.deepcopy(water_map)
    dir = (0, 1)
    current = start
    trail_map = []
    trail_mapping = {}
    last_knot = {"pos": start, "connections": []}
    trail_map.append(last_knot)
    trail_mapping[last_knot["pos"]] = last_knot
    routes = []
    water_map[start[0]][start[1]]["visited"] = True
    rec_make_map(
        water_map, trail_map, trail_mapping, current, dir, last_knot, end, routes
    )
    print_cross(trail_map)
    print_routes(trail_map, routes)
    print_visited(water_map)
    return trail_mapping, routes


trail_mapping, routes = make_map(water_map, start, end)
print(trail_mapping)


def rec_find_longest_route(
    trail_mapping, current, end, routes_taken, knots_taken, distance, routes
):
    # print("--")
    # print(trail_mapping)
    # print(current)

    current_knot = trail_mapping[current]

    # knots_taken_now.add(current)
    # print(len(current_knot["connections"]))
    for connection in current_knot["connections"]:
        # knot = connection["other_end"]

        route = connection["route"]
        knot = route["A"]
        if knot == current:
            knot = route["B"]
        else:
            assert route["B"] == current
        # if route["direction"] == -1:
        #    knot=route["B"]
        if knot not in knots_taken:
            knots_taken_now = copy.deepcopy(knots_taken)
            knots_taken_now.append(knot)
            routes_taken_now = copy.deepcopy(routes_taken)
            routes_taken_now.append(connection)
            # print(knots_taken_now)
            # print(routes_taken_now)
            # print(route["distance"])
            # print(1)
            rec_find_longest_route(
                trail_mapping,
                knot,
                end,
                routes_taken_now,
                knots_taken_now,
                route["distance"] + distance,
                routes,
            )
            # routes_taken_now.pop()

        else:
            print("shall not pass")
            # knots_taken_now.pop()
            # print(route)
            # print(routes_taken_now)
            # pass

            # print(str(route["direction"])+" "+str(knot))
            # print(0)

    if len(current_knot["connections"]) == 0:
        if current == end:
            # print_route(knots_taken)
            if len(routes) == 0:
                routes.append({"distance": distance - 1, "route": knots_taken})
            elif routes[0]["distance"] < distance - 1:
                routes[0] = {"distance": distance - 1, "route": knots_taken}
            print(
                "End "
                + str(distance - 1)
                + " max distance "
                + str(routes[0]["distance"])
            )
            print(knots_taken)
        else:
            print("Not end " + str(distance - 1))
            print(knots_taken)
            # print_route(knots_taken)
            # routes.append({"distance":distance-1, "route":knots_taken})


def find_longest_route(trail_mapping, start, end):
    routes_taken = []
    knots_taken = []
    real_routes = []
    rec_find_longest_route(
        trail_mapping, start, end, routes_taken, knots_taken, 0, real_routes
    )
    print(real_routes)
    real_routes.sort(key=lambda r: r["distance"], reverse=True)
    print(real_routes[0])
    return real_routes


find_longest_route(trail_mapping, start, end)

# 2186
# 6802
