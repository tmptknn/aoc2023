import time

with open("input.txt") as f:
    file_content = f.read().split("\n")

maze = file_content


def print_maze(maze):
    for line in maze:
        print("".join(line))


print_maze(maze)


def print_route(maze):
    for line in maze:
        pline = []
        for pos in line:
            if pos["part_route"]:
                pline.append("*")
            else:
                pline.append(" ")
        print("".join(pline))


def find_start(maze):
    for y, line in enumerate(maze):
        x = line.find("S")
        if x != -1:
            return x, y

    return -1, -1


start = find_start(maze)

print("Start " + str(start[0]) + " , " + str(start[1]))

maze_complex = []

for line in maze:
    maze_complex_line = []
    for pipe in line:
        map_pos = {
            "pipe": pipe,
            "connects": {"N": False, "S": False, "E": False, "W": False},
            "part_route": False,
        }
        connects = map_pos["connects"]

        if pipe == "|":
            connects["N"] = True
            connects["S"] = True

        if pipe == "-":
            connects["E"] = True
            connects["W"] = True

        if pipe == "L":
            connects["N"] = True
            connects["E"] = True

        if pipe == "J":
            connects["N"] = True
            connects["W"] = True

        if pipe == "7":
            connects["S"] = True
            connects["W"] = True

        if pipe == "F":
            connects["S"] = True
            connects["E"] = True

        maze_complex_line.append(map_pos)

    maze_complex.append(maze_complex_line)


if (
    start[0] < len(maze_complex[0])
    and maze_complex[start[1]][start[0] + 1]["connects"]["W"]
):
    maze_complex[start[1]][start[0]]["connects"]["E"] = True

if start[0] > 0 and maze_complex[start[1]][start[0] - 1]["connects"]["E"]:
    maze_complex[start[1]][start[0]]["connects"]["W"] = True

if start[1] > 0 and maze_complex[start[1] + 1][start[0]]["connects"]["N"]:
    maze_complex[start[1]][start[0]]["connects"]["S"] = True

if (
    start[1] > len(maze_complex)
    and maze_complex[start[1] - 1][start[0]]["connects"]["S"]
):
    maze_complex[start[1]][start[0]]["connects"]["N"] = True


current = (start[0], start[1])
last = (start[0], start[1])
step = 0


def step_pipe(maze_complex, current, last):
    for x in range(-1, 2):
        for y in range(-1, 2):
            if (
                (x == 0 and y == 0)
                or (x != 0 and y != 0)
                or current[0] + x < 0
                or current[0] + x >= len(maze_complex[0])
                or current[1] + y < 0
                or current[1] + y >= len(maze_complex)
            ):
                continue
            adjacent = maze_complex[current[1] + y][current[0] + x]["connects"]
            con = maze_complex[current[1]][current[0]]["connects"]

            if x == 1 and adjacent["W"] and con["E"] and last[0] != current[0] + 1:
                return current[0] + 1, current[1]
            if x == -1 and adjacent["E"] and con["W"] and last[0] != current[0] - 1:
                return current[0] - 1, current[1]
            if y == 1 and adjacent["N"] and con["S"] and last[1] != current[1] + 1:
                return current[0], current[1] + 1
            if y == -1 and adjacent["S"] and con["N"] and last[1] != current[1] - 1:
                return current[0], current[1] - 1

    assert False


while True:
    previous = (current[0], current[1])
    current = step_pipe(maze_complex, current, last)
    maze_complex[current[1]][current[0]]["part_route"] = True

    last = (previous[0], previous[1])
    step += 1
    if current[0] == start[0] and current[1] == start[1]:
        break

print_route(maze_complex)
print(int(step / 2))


def calculate_maze_complex_area(maze):
    sum = 0
    for line in maze:
        inside = False
        for pos in line:
            if pos["part_route"] and pos["connects"]["S"]:
                inside = not inside

            if inside and not pos["part_route"]:
                pos["area"] = True
            else:
                pos["area"] = False

    for line in maze:
        pline = []
        for pos in line:
            if pos["part_route"]:
                pline.append(pos["pipe"])
            else:
                if pos["area"]:
                    sum += 1
                    pline.append(".")
                else:
                    pline.append(" ")
        print("".join(pline))
    return sum


print(calculate_maze_complex_area(maze_complex))
