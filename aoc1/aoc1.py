with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()

digits = "0123456789"


def find_first_digit(line):
    for i in range(0, len(line)):
        if line[i] in digits:
            return line[i]


def find_last_digit(line):
    for i in range(len(line) - 1, -1, -1):
        if line[i] in digits:
            return line[i]


def find_coordinate(line):
    f = find_first_digit(line)
    l = find_last_digit(line)
    return int("" + f + l)


def calculate_sum(lines):
    sum = 0
    for line in lines:
        sum += find_coordinate(line)

    print(sum)


calculate_sum(lines)
