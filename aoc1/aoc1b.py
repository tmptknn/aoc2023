with open("input.txt") as f:
    filecontent =f.read()

lines = filecontent.splitlines();

letter_digits =["one","two","three","four","five","six","seven","eight", "nine"]
digits = "0123456789"

def find_first_digit(line):
    for i in range(0,len(line)):
        if line[i] in digits:
            return line[i]
        for j in range(0,len(letter_digits)):
            if line[i:].startswith(letter_digits[j]):
                return str(j+1)

        
def find_last_digit(line):
    for i in range(len(line)-1,-1,-1):
        if(line[i] in digits):
            return line[i]
        for j in range(0,len(letter_digits)):
            if line[i:].startswith(letter_digits[j]):
                return str(j+1)
        
def find_coordinate(line):
    f = find_first_digit(line)
    l = find_last_digit(line)
    return int(""+f+l)

def calculate_sum(lines):
    sum = 0
    for line in lines:
        sum+=find_coordinate(line)
    
    print (sum)

calculate_sum(lines)