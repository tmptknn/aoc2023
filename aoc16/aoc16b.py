import copy

with open("input.txt") as f:
    file_content = f.read().split("\n")

mappi = []
for row in file_content:
    mappi.append([{"type": s, "energized": []} for s in row])


# print(mappi)
def step_beams(mappi, posx, posy, dirx, diry):
    stack = [(posx, posy, dirx, diry)]
    while True:
        if len(stack) == 0:
            return mappi
        posx, posy, dirx, diry = stack.pop()
        if (
            posx < 0
            or posx >= len(mappi[0])
            or posy < 0
            or posy >= len(mappi)
            or (dirx, diry) in mappi[posy][posx]["energized"]
        ):
            continue
        mappi[posy][posx]["energized"].append((dirx, diry))

        tile_type = mappi[posy][posx]["type"]
        if tile_type == "|" and dirx != 0:
            stack.append((posx, posy - 1, 0, -1))
            stack.append((posx, posy + 1, 0, 1))
        if tile_type == "-" and diry != 0:
            stack.append((posx - 1, posy, -1, 0))
            stack.append((posx + 1, posy, 1, 0))
        if tile_type == "\\":
            stack.append((posx + diry, posy + dirx, diry, dirx))
        if tile_type == "/":
            stack.append((posx - diry, posy - dirx, -diry, -dirx))
        if (
            tile_type == "."
            or (dirx != 0 and tile_type == "-")
            or (diry != 0 and tile_type == "|")
        ):
            stack.append((posx + dirx, posy + diry, dirx, diry))


def calc_energized(mappi):
    sum = 0
    for y in range(len(mappi)):
        for x in range(len(mappi[0])):
            if len(mappi[y][x]["energized"]) > 0:
                sum += 1
    return sum


def print_map(mappi):
    sum = 0
    for y in range(len(mappi)):
        pline = ""
        for x in range(len(mappi[0])):
            if len(mappi[y][x]["energized"]) > 0:
                pline += "#"
                sum += 1
            else:
                pline += "."
            #    sum += 1
            # print(mappi[y][x])
        print(pline)
    return sum


sum = 0
for i in range(len(mappi[0])):
    sum = max(sum, calc_energized(step_beams(copy.deepcopy(mappi), i, 0, 0, 1)))
    sum = max(
        sum, calc_energized(step_beams(copy.deepcopy(mappi), i, len(mappi) - 1, 0, -1))
    )

for i in range(len(mappi)):
    sum = max(sum, calc_energized(step_beams(copy.deepcopy(mappi), 0, i, 1, 0)))
    sum = max(
        sum,
        calc_energized(step_beams(copy.deepcopy(mappi), len(mappi[0]) - 1, i, -1, 0)),
    )

print(sum)
# print_map(mappi)
