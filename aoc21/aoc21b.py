import copy
import time

with open("input.txt") as f:
    lines = f.read().split("\n")


garden_plot = []
start = None
for y, line in enumerate(lines):
    garden_line = []
    for x, pos in enumerate(line):
        spot = {"symbol": pos, "x": x, "y": y, "visited": False}
        if pos == "S":
            spot["symbol"] = "."
            start = spot

        garden_line.append(spot)
    garden_plot.append(garden_line)

# print(garden_plot)

steps = [(1, 0), (-1, 0), (0, 1), (0, -1)]


def valid_pos(garden_plot, x, y):
    if x < 0 or x >= len(garden_plot[0]):
        return False
    if y < 0 or y >= len(garden_plot):
        return False
    return True


def print_situation(garden_plot):
    for line in garden_plot:
        pline = ""
        for spot in line:
            symbol = spot["symbol"]
            if spot["visited"]:
                symbol = "O"
            pline += symbol
        print(pline)
    print("")


def print_big_situation(maps, garden_plot):
    for y in range(-2, 3):
        for i, line in enumerate(range(len(garden_plot))):
            pline = ""
            for x in range(-2, 3):
                map_key = str((x, y))
                if map_key in maps.keys():
                    current_map = maps[map_key]["map"]
                else:
                    current_map = garden_plot
                current_plot_line = current_map[i]
                for spot in current_plot_line:
                    symbol = spot["symbol"]
                    if spot["visited"]:
                        symbol = "O"
                    pline += symbol
            print(pline)
    print("")


def solve(x, a0,a1,a2):
    b0 = a0
    b1 = a1-a0
    b2 = a2-a1
    return b0 + b1*x + (x*(x-1)//2)*(b2-b1)


def step_loop_close_but_no_hit(garden_plot_in, startx, starty, max_depth):
    depth = 0
    points=[]
    maps = {
        str((0, 0)): {
            "saturated": False,
            "all_saturated": False,
            "next_spots": [(startx, starty)],
            "new_spots": [],
            "sums": [],
            "pos": (0, 0),

        }
    }
    total_sums = []
    while depth < max_depth + 1:
        total_sum = 0
        total_sum1 = 0
        total_sum2 = 0
        new_maps = {}

        for map_key in maps:
            all_saturated = True
            current_map = maps[map_key]

            for st in steps:
                spos = current_map["pos"]
                smap_key = (spos[0] + st[0], spos[1] + st[1])
                if smap_key in maps:
                    if not maps[smap_key]["saturated"]:
                        all_saturated = False
                        break
                else:
                    all_saturated = False
                    break

            if all_saturated:
                current_map["all_saturated"] = True
                
            current_map["map"]=copy.deepcopy(garden_plot_in)


        for map_key in maps:
            sum = 0
            sum1=0
            sum2 =0
            
            current_map = maps[map_key]

            if current_map["all_saturated"]:
                if depth % 2 == 0:
                    sum = current_map["saturated_0"]
                    continue
                else:
                    sum = current_map["saturated_1"]
                    continue
            # current_map["map"] = garden_plot
            garden_plot=current_map["map"]
            spots = current_map["next_spots"]
            next_spots = []

            while len(spots) > 0:
                px, py = spots.pop()
                # print(px)
                # print(py)
                # print(len(garden_plot[0]))
                # print(len(garden_plot))
                spot = garden_plot[py][px]

                if spot["visited"]:
                    continue
                else:
                    spot["visited"] = True

                if spot["symbol"] == "#":
                    continue

                sum += 1
                dx =px- startx
                dy =py -starty
                if abs(dx)+abs(dy)<=depth-1:
                    sum1 +=1
                if abs(dx)+abs(dy)<=depth-2:
                    sum2 +=1    

                for step in steps:
                    if valid_pos(garden_plot, px + step[0], py + step[1]):
                        next_spots.append((px + step[0], py + step[1]))
                    else:
                        posx, posy = current_map["pos"]
                        posx += step[0]
                        posy += step[1]
                        add_map_key = str((posx, posy))
                        if add_map_key in maps.keys():
                            other_map = maps[add_map_key]
                            if other_map["saturated"]:
                                pass
                            else:
                                other_map["new_spots"].append(
                                    (
                                        (px + step[0]) % len(garden_plot[0]),
                                        (py + step[1]) % len(garden_plot),
                                    )
                                )
                        else:
                            if add_map_key in new_maps.keys():
                                other_map = new_maps[add_map_key]
                                if other_map["saturated"]:
                                    pass
                                else:
                                    other_map["new_spots"].append(
                                        (
                                            (px + step[0]) % len(garden_plot[0]),
                                            (py + step[1]) % len(garden_plot),
                                        )
                                    )
                            else:
                                new_maps[add_map_key] = {
                                    "saturated": False,
                                    "all_saturated": False,
                                    "next_spots": [
                                        (
                                            (px + step[0]) % len(garden_plot[0]),
                                            (py + step[1]) % len(garden_plot),
                                        )
                                    ],
                                    "new_spots": [],
                                    "sums": [],
                                    "pos": (posx, posy),
                                }

            current_map["next_spots"] = next_spots
            # print(current_map)
            if len(current_map["sums"]) > 1 and sum == current_map["sums"][-2] and not current_map["saturated"] :
                current_map["saturated"] = True
                print("saturated "+str(depth)+" "+str(sum)+" "+str(current_map["sums"][-1]))
                if depth % 2 == 0:
                    current_map["saturated_0"] = sum
                    current_map["saturated_1"] = current_map["sums"][-1]
                else:
                    current_map["saturated_1"] = sum
                    current_map["saturated_0"] = current_map["sums"][-1]
            current_map["sums"].append(sum)

            # print_situation(garden_plot)
            total_sum += sum
            total_sum1 +=sum1
            total_sum2 +=sum2

        print(str(depth) + " " + str(total_sum))
        # print_big_situation(maps, garden_plot_in)
        # time.sleep(0.25)
        for new_map_key in new_maps.keys():
            maps[new_map_key] = new_maps[new_map_key]
        for some_map_key in maps.keys():
            if not maps[some_map_key]["saturated"]:
                maps[some_map_key]["next_spots"].extend(maps[some_map_key]["new_spots"])
                maps[some_map_key]["new_spots"] = []
        depth += 1
        for sat_index, sum in enumerate(total_sums):
            if sum * 9 == total_sum and depth >= len(garden_plot_in[0]) - 1:
                return depth, sat_index + 1, sum, total_sum
        if len(total_sums)>2 and total_sum2 == total_sums[-2]:
            print("saturated at distance "+str(depth-2))
            #if depth-2 == len(garden_plot_in):
        if depth%len(garden_plot) == max_depth%len(garden_plot):
            points.append(total_sum)
            if len(points) ==3:
                return solve(max_depth//len(garden_plot),*points)
        total_sums.append(total_sum)
        

    return result


def step_loop(garden_plot, start_x,start_y, max_steps):
    steps_taken=0
    spots=set()
    points =[]
    spots.add((start_x,start_y))
    while steps_taken < max_steps:
        new_spots = set()
        for x,y in spots:
            for step in steps:
                tx = x+step[0]
                ty = y+step[1]
                if garden_plot[ty%len(garden_plot)][tx%len(garden_plot[0])]["symbol"] != "#":
                    new_spots.add((tx,ty))
        spots = new_spots
        steps_taken+=1
        total_sum = len(spots)
        if steps_taken%len(garden_plot) == max_steps%len(garden_plot):
            points.append(total_sum)
            if len(points) ==3:
                return solve(max_steps//len(garden_plot),*points)

print(step_loop(garden_plot, start["x"], start["y"], 26501365))
