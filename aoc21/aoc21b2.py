with open("input.txt") as f:
    lines = f.readlines()


garden_plot = []
start = None
for y, line in enumerate(lines):
    garden_line = []
    for x, pos in enumerate(line):
        spot = {"symbol": pos, "x": x, "y": y, "visited": 65 * [0]}
        if pos == "S":
            spot["symbol"] = "."
            start = spot

        garden_line.append(spot)
    garden_plot.append(garden_line)

print(garden_plot)

steps = [[1, 0], [-1, 0], [0, 1], [0, -1]]


def valid_pos(garden_plot, x, y):
    if x < 0 or x >= len(garden_plot[0]):
        return False
    if y < 0 or y >= len(garden_plot):
        return False
    return True


def step_rec(garden_plot, px, py, depth, max_depth):
    sum = 0
    spot = garden_plot[py][px]
    if spot["visited"][depth] != 0:
        return 0
    else:
        spot["visited"][depth] = 1

    if spot["symbol"] == "#":
        return 0

    if depth == max_depth - 1:
        if spot["symbol"] == ".":
            return 1

    for step in steps:
        if valid_pos(garden_plot, px + step[0], py + step[1]):
            sum += step_rec(
                garden_plot, px + step[0], py + step[1], depth + 1, max_depth
            )

    return sum


print(step_rec(garden_plot, start["x"], start["y"], -1, 64))


def calc_spots(garden_plot):
    sum = 0
    for line in garden_plot:
        for spot in line:
            visited = False
            for i in range(64):
                if spot["visited"][i] != 0:
                    visited = True
            if visited:
                sum += 1

    return sum


print(calc_spots(garden_plot))
