import json

with open("input.txt") as f:
    filecontent = f.read()

card_order = "AKQT98765432J"

hand_order = ["5ofKind", "4ofKind","fullHouse","3OfKind", "twoPair","onePair","highCard"]

lines = filecontent.splitlines()

games = []

def get_hand_type(hand):
    map = {}
    jokers = 0
    for card in hand:
        if card=="J":
            jokers+=1
        elif card in map.keys():
            map[card]+=1
        else:
            map[card]=1

    most = 0
    second_most = 0
    for key in map.keys():
        if most<= map[key]:
            second_most = most
            most=map[key]
        else:
            if second_most <= map[key]:
                second_most = map[key]
    most +=jokers
    if most == 5:
        return hand_order[0], 0
    elif most == 4:
        return hand_order[1], 1
    elif most == 3 and second_most == 2:
        return hand_order[2], 2
    elif most == 3:
        return hand_order[3], 3
    elif most == 2 and second_most == 2:
        return hand_order[4], 4
    elif most == 2:
        return hand_order[5], 5 
    else:
        return hand_order[6], 6

def get_card_values(hand):
    cards =[]
    for card in hand:
        cards.append(card_order.find(card))
    return cards


for line in lines:
    hand_and_value = line.split()
    hand = hand_and_value[0]
    value = int(hand_and_value[1])
    hand_type, hand_value = get_hand_type(hand)
    card_values = get_card_values(hand)
    game = {"hand":hand, "value":value, "hand_type":hand_type,"hand_value":hand_value, "card_values":card_values}
    games.append(game)

#print(games)

def sort_by_value_index(game,index):
    return game["card_values"][index]

def sort_by_value(games):
    sorted_values = sorted(games,key=lambda game: sort_by_value_index(game,4), reverse=True)
    for i in range(4,-1,-1):
        sorted_values.sort(key=lambda game: sort_by_value_index(game, i), reverse=True)
    return sorted_values


sorted_values = sort_by_value(games)
print(json.dumps(sorted_values,indent=3))

sorted_games = sorted( sorted_values,key=lambda game: game["hand_value"], reverse=True)

sum = 0
for j, hand in enumerate(sorted_games):
    sum += hand["value"]*(j+1)
print(json.dumps(sorted_games, default=str, indent=3))
print(sum)