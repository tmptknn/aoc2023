from datetime import datetime
import math

with open("input.txt") as f:
    instructions = f.readline()[:-1]
    f.readline()
    filecontent = f.read().split("\n")

map = {}
for mapline in filecontent:
    pos_dir = mapline.split(" = ")
    pos = pos_dir[0]
    LR = pos_dir[1].split(", ")
    L = LR[0][1:]
    R = LR[1][:-1]
    map[pos] = {"L":L,"R":R}

start_time = datetime.now()
currents = []
ends = []

for key in map.keys():
    if key[2] == "A":
        currents.append(key)
        ends.append([])

def end_is_nigh(ends):
    for end in ends:
        if len(end) ==0:
            return False
    return True

def steps(ends):
    nomi = []
    for end in ends:
        nomi.extend(end)
    return math.lcm(*nomi)


linstr = len(instructions)
def find_end():
    step=0
    while True:
        for index, current in enumerate(currents):
            if current[2] == "Z": 
                ends[index].append(step)
                if end_is_nigh(ends):
                    return
            currents[index] = map[current][instructions[step%linstr]]
        step += 1

find_end()
end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))
print(steps(ends))