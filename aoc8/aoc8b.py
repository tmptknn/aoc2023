import math

with open("input.txt") as f:
    instructions = f.readline()[:-1]
    f.readline()
    filecontent = f.read().split("\n")

map = {}
for mapline in filecontent:
    pos_dir = mapline.split(" = ")
    pos = pos_dir[0]
    LR = pos_dir[1].split(", ")
    L = LR[0][1:]
    R = LR[1][:-1]
    map[pos] = {"L":L,"R":R}

# print(instructions)
# print(map)
currents = []


for key in map.keys():
    if key[2] == "A":
        currents.append(key)

ends =[[] for current in currents]

def end_is_nigh(ends):
    for end in ends:
        if len(end) ==0:
            return False

    return True

def steps(ends):
    nomi = []
    for end in ends:
        for ending in end:
            nomi.append(ending)

    return math.lcm(*nomi)

step = 0
while not end_is_nigh(ends):
    for index, current in enumerate(currents):
        if current[2] == "Z": 
            ends[index].append(step)
        currents[index] = map[current][instructions[step%len(instructions)]]
    step += 1

print(steps(ends))