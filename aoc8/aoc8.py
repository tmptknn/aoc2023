from datetime import datetime
with open("input.txt") as f:
    instructions = f.readline()[:-1]
    f.readline()
    filecontent = f.read().split("\n")

map = {}
for mapline in filecontent:
    pos_dir = mapline.split(" = ")
    pos = pos_dir[0]
    LR = pos_dir[1].split(", ")
    L = LR[0][1:]
    R = LR[1][:-1]
    map[pos] = {"L":L,"R":R}

# print(instructions)
# print(map)
start_time = datetime.now()
current = "AAA"
step = 0
while current != "ZZZ":
    current = map[current][instructions[step%len(instructions)]]
    step += 1
end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))
print(step)