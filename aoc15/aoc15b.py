with open("input.txt") as f:
    file_content = f.read()

code = file_content.split(",")
boxes = []
for i in range(256):
    boxes.append([])
print(boxes)
print(code)

def print_boxes(boxes):
    for boxnumber,box in enumerate(boxes):
        if len(box) == 0:
            continue
        print("box"+str(boxnumber))
        for lens in box:
            print(lens)

def sum_boxes(boxes):
    sum=0
    for boxnumber,box in enumerate(boxes):
        if len(box) == 0:
            continue
        #print("box"+str(boxnumber))
        box_multiplier = boxnumber+1
        for lens_number,lens in enumerate(box):
            sum+=box_multiplier*(lens_number+1)*lens["focal_length"]
    return sum



for s in code:
    print(s)
    current_value =0
    label = ""
    for i,c in enumerate(s):
        if c=="-":
            label =s[:i]
            #print(label)
            box_number = current_value
            #print(box_number)
            box=boxes[box_number]
            for j,lens in enumerate(box):
                if lens["label"] == label:
                    box.pop(j)
            break
        if c=="=":
            label = s[:i]
            #print(label)
            focal_length =int(s[i+1:])
            #print(focal_length)
            box_number = current_value
            #print(box)
            box = boxes[box_number]
            found =False
            for j,lens in enumerate(box):
                if lens["label"] == label:
                    box[j] = {"label":label, "focal_length":focal_length}
                    found = True
            if not found:
                box.append({"label":label,"focal_length":focal_length})
            break
        current_character=ord(c)
        current_value+=current_character
        current_value*=17
        current_value=current_value%256

    print_boxes(boxes)

print(sum_boxes(boxes))
