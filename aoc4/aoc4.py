with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()

sum = 0

for line in lines:
    game_number_and_correct_numbers = line.split(": ")
    game_number = game_number_and_correct_numbers[0]
    correct_numbers_and_numbers = game_number_and_correct_numbers[1].split(" | ")
    correct_numbers = " ".join(correct_numbers_and_numbers[0].split()).split()
    numbers = " ".join(correct_numbers_and_numbers[1].split()).split()
    print(correct_numbers)
    print(numbers)
    correct_numbers_list = []
    for correct_number in correct_numbers:
        correct_numbers_list.append(int(correct_number))

    times = 0
    for number in numbers:
        if int(number) in correct_numbers_list:
            times += 1

    if times > 0:
        sum += pow(2, (times - 1))

print(sum)
