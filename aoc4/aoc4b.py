with open("input.txt") as f:
    filecontent = f.read()

lines = filecontent.splitlines()

sum = 0

multiplier_list = [1 for line in lines]
print(multiplier_list)
for line in lines:
    game_number_and_correct_numbers = line.split(": ")
    game_number = int(game_number_and_correct_numbers[0][5:])
    correct_numbers_and_numbers = game_number_and_correct_numbers[1].split(" | ")
    correct_numbers = " ".join(correct_numbers_and_numbers[0].split()).split()
    numbers = " ".join(correct_numbers_and_numbers[1].split()).split()
    print(correct_numbers)
    print(numbers)
    correct_numbers_list = []
    for correct_number in correct_numbers:
        correct_numbers_list.append(int(correct_number))

    times = 0
    for number in numbers:
        if int(number) in correct_numbers_list:
            times += 1

    for i in range(game_number, game_number + times):
        multiplier_list[i] += multiplier_list[game_number - 1]

    sum += multiplier_list[game_number - 1]

print(multiplier_list)
print(sum)
